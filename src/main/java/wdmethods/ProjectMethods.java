package wdmethods;

import java.io.IOException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;

import utils.ReadingExcel;

public class ProjectMethods extends SeMethods {
	@BeforeMethod
	public void login() {
		startApp("firefox", "http://leaftaps.com/opentaps");
		/*WebElement eleUserName = locateElement("id", "username");
		type(eleUserName, "DemoSalesManager");
		WebElement elePassword = locateElement("password");
		type(elePassword, "crmsfa");
		WebElement eleLogin = locateElement("class","decorativeSubmit");
		click(eleLogin);	*/

}
	@DataProvider(name="fetchData")
	public Object[][] getData() throws IOException
	{
	    Object[][] sheet = ReadingExcel.readingXL(dataSheetName);
		return sheet;
	
	}
	@AfterMethod
	public void close()
	{
		closeBrowser();
	} 	
}
