package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pAGEs.LoginPage;
import wdmethods.ProjectMethods;

public class TC001_CreateLead extends ProjectMethods{
	@BeforeTest
	public void setData()
	{
		testCaseName="LearnPage";
		testCaseDesc = "Login into LeafTap";
		category = "SIT";
		author = "Murali Baskaran";
		dataSheetName="Login";
	}
	@Test(dataProvider="fetchData")
	public void LoginPage(String uName, String password, String cname, String fname, String lname)
	{
		new LoginPage()
		.enterUsername(uName)
		.enterPassword(password)
		.clickLogin()
		.clicCrmsfa()
		.clickLeads()
		.clickCreateLead()
		.enterCompanyName(cname)
		.enterFirstName(fname)
		.enterLastName(lname)
		.clickCreateButton()
		.verifyFirstName(fname);
	}
	
}







