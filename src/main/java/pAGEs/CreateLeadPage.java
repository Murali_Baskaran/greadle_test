package pAGEs;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdmethods.ProjectMethods;

public class CreateLeadPage extends ProjectMethods {
	
	
	public CreateLeadPage()
	{
		PageFactory.initElements(driver, this);
	}
	@FindBy(how=How.ID, using = "createLeadForm_firstName") WebElement eleFirstName;
	@FindBy(how=How.ID, using = "createLeadForm_lastName") WebElement eleLastName;
	@FindBy(how=How.ID, using = "createLeadForm_companyName") WebElement eleCompanyName;
	@FindBy(how=How.CLASS_NAME, using = "smallSubmit") WebElement eleCreateLeadClick;
	
	
		public CreateLeadPage enterFirstName(String firstName)
	  {
		  type(eleFirstName,firstName);
		  return this;
		  
	  }
		public CreateLeadPage enterLastName(String lastName)
		  {
			  type(eleLastName,lastName);
			  return this;
			  
		  }
		public CreateLeadPage enterCompanyName(String companyName)
		  {
			  type(eleCompanyName,companyName);
			  return this;
			  
		  }

		public ViewLeadPage clickCreateButton()
		{
			click(eleCreateLeadClick);
			return new ViewLeadPage();
	
		}
}
